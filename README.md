# README #

### What is this repository for? ###

A simple To Do Application with an Angular 1 component perspective

### How do I get set up? ###

Make sure you have Bower, Mongo and Node installed.
Then just:

* Clone the repo
* npm install
* bower install

### How to run it? ###

* node server.js

And then the magic will be on: http://localhost:8080/

### Who do I talk to? ###

* Repo owner
