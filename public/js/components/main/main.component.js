(function() {

  'use strict';

  angular.module('todoApp').component('toDoContainer', {
    templateUrl: 'js/components/main/views/transaction.html',
    controller: ToDoContainerController
  });

  ToDoContainerController.$inject = ['todoService'];
  function ToDoContainerController(todoService) {
    var vm = this;

    angular.extend(vm, {
      DATA: todoService.shared
    });

    vm.$onInit = init;

    function init() {
      todoService.getTodos();
    }

  }


})();
