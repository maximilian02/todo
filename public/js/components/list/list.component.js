(function() {

  'use strict';

  angular.module('todoApp').component('toDoList', {
    templateUrl: 'js/components/list/views/transaction.html',
    controller: ToDoListController,
    controllerAs: 'vm',
    require: {
      parent: '^^toDoContainer'
    }
  });

  ToDoListController.$inject = ['todoService'];
  function ToDoListController(todoService) {
    var vm = this;

    angular.extend(vm, {
      doneTodo: doneTodo,
      deleteTodo: deleteTodo
    });

    function doneTodo(todo) {
      // Yeah, i know, instead of getting the entire todo object, why not send only the id?
      // I want to avoid weird users looking the view and seeing how I access to t._id
      // Of course this can be avoided applying some mongo magic, but don't have time for that
      // At least at this moment tho.
      todoService.updateTodo(todo._id)
        .then(function(res) {
          todoService.shared.todos = res;
        });
    }

    function deleteTodo(todo) {
      todoService.deleteTodo(todo._id)
        .then(function(res) {
          todoService.shared.todos = res;
        });
    }

  }


})();
