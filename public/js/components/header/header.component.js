(function() {

  'use strict';

  angular.module('todoApp').component('toDoHeader', {
    templateUrl: 'js/components/header/views/transaction.html',
    controller: ToDoHeaderController,
    controllerAs: 'vm'
  });

  ToDoHeaderController.$inject = ['todoService'];
  function ToDoHeaderController(todoService) {
    var vm = this;

    angular.extend(vm, {
      addTodo: addTodo,
      todoText: ''
    });

    function addTodo() {
      todoService.createTodo({ text: vm.todoText })
        .then(function(res) {
          todoService.shared.todos = res;
          vm.todoText = '';
        });
    }
  }


})();
