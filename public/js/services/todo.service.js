(function(){
	'use strict';

	angular.module('todoApp').service('todoService', todoService);

	todoService.$inject = ['$q', '$http'];
	function todoService($q, $http){
    var apiUrl = 'http://localhost:8080/api/todos/';
    var service = {
      shared: {
        todos: []
      },
      getTodos: getTodos,
      createTodo: createTodo,
      updateTodo: updateTodo,
      deleteTodo: deleteTodo
    };

    return service;

    function getTodos() {
      $http.get(apiUrl).then(function(res) {
        service.shared.todos = res.data;
      });
    }

    function createTodo(data) {
      var deferred = $q.defer();

      $http.post(apiUrl, data).then(function(res) {
        deferred.resolve(res.data);
      });

      return deferred.promise;
    }

    function updateTodo(id) {
      var deferred = $q.defer();

      $http.put(apiUrl + id).then(function(res) {
        deferred.resolve(res.data);
      });

      return deferred.promise;
    }

    function deleteTodo(id) {
      var deferred = $q.defer();

      $http.delete(apiUrl + id).then(function(res) {
        deferred.resolve(res.data);
      });

      return deferred.promise;
    }

  }

})();
