var Todo = require('./models/todo');

module.exports = function(app) {
    // API routes

    // GET All Todos
    app.get('/api/todos', function(req, res) {
        Todo.find(function(err, todos) {
            if (err) { res.send(err); }
            res.json(todos);
        });
    });

    // POST a Todo
    app.post('/api/todos', function(req, res) {
        Todo.create({
            text : req.body.text,
            done : false
        }, function(err, todo) {
            if (err) { res.send(err); }

            // Everything looks okay? Lets return an updated list of todos
            Todo.find(function(err, todos) {
                if (err) { res.send(err); }
                res.json(todos);
            });
        });
    });

    // UPDATE a Todo
    app.put('/api/todos/:todoId', function(req, res) {
      Todo.findById(req.params.todoId, function(err, t) {
        if (!t) {
            return next(new Error('Could not load Document'));
        } else {
          t.done = !t.done;
          t.save(function(err) {
            if (err) { res.send(err); }

            // Everything looks okay? Lets return an updated list of todos
            Todo.find(function(err, todos) {
                if (err) { res.send(err); }
                res.json(todos);
            });
          });
        }
      });
    });

    // DELETE a Todo
    app.delete('/api/todos/:todoId', function(req, res) {
        Todo.remove({
            _id : req.params.todoId
        }, function(err, todo) {
            if (err) { res.send(err); }

            // Everything looks okay? Lets return an updated list of todos
            Todo.find(function(err, todos) {
                if (err) { res.send(err); }
                res.json(todos);
            });
        });
    });

    // Serve with the AngularJS application
    app.get('*', function(req, res) {
      res.sendfile('./public/index.html');
    });

};
